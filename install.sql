ALTER TABLE products_attributes ADD products_attributes_length_prefix char(1) NOT NULL DEFAULT '';
ALTER TABLE products_attributes ADD products_attributes_length float NOT NULL DEFAULT '0';
ALTER TABLE products_attributes ADD products_attributes_width_prefix char(1) NOT NULL DEFAULT '';
ALTER TABLE products_attributes ADD products_attributes_width float NOT NULL DEFAULT '0';
ALTER TABLE products_attributes ADD products_attributes_height_prefix char(1) NOT NULL DEFAULT '';
ALTER TABLE products_attributes ADD products_attributes_height float NOT NULL DEFAULT '0';

ALTER TABLE orders_products_attributes ADD products_attributes_length_prefix char(1) NOT NULL DEFAULT '';
ALTER TABLE orders_products_attributes ADD products_attributes_length float NOT NULL DEFAULT '0';
ALTER TABLE orders_products_attributes ADD products_attributes_width_prefix char(1) NOT NULL DEFAULT '';
ALTER TABLE orders_products_attributes ADD products_attributes_width float NOT NULL DEFAULT '0';
ALTER TABLE orders_products_attributes ADD products_attributes_height_prefix char(1) NOT NULL DEFAULT '';
ALTER TABLE orders_products_attributes ADD products_attributes_height float NOT NULL DEFAULT '0';