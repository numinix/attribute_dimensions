Installation instruction

1. Backup your database and all files.
2. Install Numinix Product Fields
3. Rename all folders in the package named YOUR_ADMIN to your custom admin folder name.
4. Run install.sql in Admin Tools Install SQL Patches.
5. Merge the changes in the installation files.
6. To be able to set dimensions for each product, add products_length, products_width, and products_height using Admin > Catalog > Product Numinix Field.